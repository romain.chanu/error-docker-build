# Stage 1: Build stage
FROM golang:latest as builder

WORKDIR /app

# Copy only the necessary files for downloading dependencies.
COPY go.mod .
COPY go.sum .

# Download dependencies first to leverage Docker cache.
RUN go mod download

# Copy the entire application source code.
COPY . .

# Build the application.
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# Stage 2: Final image
FROM alpine:latest

# Install CA certificates required for HTTPS communication.
RUN apk --no-cache add ca-certificates

WORKDIR /app

# Copy the built binary from the builder stage.
COPY --from=builder /app/main .

# Expose the port on which the application will run.
EXPOSE 3000

# Define the command to run the application.
CMD ["./main"]
